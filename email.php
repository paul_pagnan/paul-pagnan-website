<?php
    require_once('bower_components/phpmailer/class.phpmailer.php');
    
    $error = true;
    $robot = "";

    $email = $_POST['email'];
    $name = $_POST['name'];
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    if(isset($_POST['name'])) {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $fields = array(
            'secret' => "6LdJpQQTAAAAADLe872GzXLltaHqeGINXyXuhtm3",
            'response' => $_POST['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        );

        //url-ify the data for the POST
        $fields_string = "";
        foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
        rtrim($fields_string, '&');
        //open connection
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_POST, count($fields));
        curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //execute post
        $result = curl_exec($ch);
        //close connection
        curl_close($ch);

        $decode = json_decode($result, true);
        
        if($decode['success']) {
            $mail = new PHPMailer();

            $mail->IsSMTP();                                      // set mailer to use SMTP
            $mail->Host = "localhost";  // specify main and backup server

            $mail->From = $_POST['email'];
            $mail->FromName = $_POST['name'];
            $mail->AddAddress("paul@pagnan.com.au");

            $mail->WordWrap = 50;                                 // set word wrap to 50 characters
            $mail->IsHTML(true);                                  // set email format to HTML
            $mail->Subject = "Website Contact - " . $_POST['subject'];
            $mail->Body    =  $_POST['message'];

            if($mail->Send())
            {
                $error = false;
            }
        }
        else
        {
            $robot = "Failed recapture <br><br>";
        }
    }
?>

<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Paul Pagnan</title>
        <link rel="stylesheet" href="stylesheets/app.css" />
        <link rel="icon" type="image/png" href="favicon.png">
        <script src="bower_components/modernizr/modernizr.js"></script>
        <script src='https://www.google.com/recaptcha/api.js?onload=watchRecapture'></script>
    </head>
    <body> 
       
        
        <div class="hero skrollable skrollable-between" id="top" data-start="opacity: 1;" data-100p="opacity:0.2;" style="opacity: 1;">
            <div class="spacer"></div>
            <div class="contain-to-grid sticky">
                <nav class="top-bar" data-topbar role="navigation">
                    <ul class="title-area">
                        <li class="name">
                            <a href="#top"><img class="logo" src="images/Logo.svg" /></a>
                        </li>
                        <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
                        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
                    </ul>
                    <section class="top-bar-section">
                        <!-- Right Nav Section -->
                        <ul class="right">
                            <li><a href="/">Home</a></li>
                            <li><a href="/#profile">Profile</a></li>
                            <li><a href="/#skills">Skills</a></li>
                            <li><a href="/#education">Education</a></li>
                            <li><a href="/#experience">Experience</a></li>
                            <li class="active"><a href="/#contact">Contact</a></li>
                        </ul>
                    </section>
                </nav>
            </div>
            <div class="hero-content-container" data-sr>
                <div class="hero-content">
                    <div class="row">
                        <div class="medium-10 small-12 small-centered">
                            <?php if($error) { ?>
                                <h1><span>Oh no!</span></h1>
                                <h3 class="white">Something went wrong, lets try that again</h3><br>  
                                <div class="row">
                                    <div class="small-12 text-center">
                                        <form action="email.php" method="post" id="contact" class="no-padding" >
                                            <div class="row">
                                                <div class="medium-6 columns">
                                                    <input type="text" value="<?php echo $name ?>" name="name" placeholder="Name">
                                                    <input type="text" value="<?php echo $subject ?>" name="subject" placeholder="Subject">
                                                    <input type="email" value="<?php echo $email ?>" name="email" placeholder="Email">
                                                </div>
                                                <div class="medium-6 columns">
                                                    <textarea rows=7 name="message" placeholder="Message"><?php echo $message ?></textarea>
                                                    <div class="row">
                                                        <div class="g-recaptcha right" data-sitekey="6LdJpQQTAAAAAJzXDj3PhtxwEYO5rsI62oseuTqU" ></div>
                                                    </div>
                                                    <div class="row">
                                                        <span class="right red"><?php echo $robot; ?></span>
                                                    </div>
                                                    <button type="submit" class="right button tiny radius">Send</button>
                                                </div>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                                      
                            <?php   } else { ?>
                                <h1><span>Thankyou!</span></h1>
                                <h3 class="white">Your message was sent successfully</h3><br>
                                <a href="/" class="button small radius"><i class="fa fa-arrow-left"></i> &nbsp; Back</a>
                            <?php } ?>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/foundation/js/foundation.min.js"></script>
        <script src="bower_components/scrollReveal.js/scrollReveal.js"></script>
        <script src="js/common.js"></script>
    </body>
</html>