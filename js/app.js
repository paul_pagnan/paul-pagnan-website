// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();

//window.sr = new scrollReveal();

$(document).ready(function() {    
    $('[data-typer-targets]').typer();
    $('a[href^="#"]').on('click',function (e) {
        //if(!isSkill($(this.hash))) {
            e.preventDefault();
            scrollTo(this.hash);
        //}
    });
    $(".skills-buttons a").on('click', function(e) {
        e.preventDefault();
        var target = e.target;
        tabs($(target).attr("href"));
    });
    
    
    drawGraphs();

    var str = document.URL;
    var url = str.split("/");
    var target = url[url.length - 1];
    if(url.length > 1) {
        scrollTo(target);
    }
    if(isSkill($(target)))
        tabs(target);   
});


$(window).resize(function() {
    if (Modernizr.mq('(max-width: 500px)')) {
        drawGraphs();
    } 
});


function tabs(id) {
    $(".skills-buttons a").removeClass("active");
    $(id + "-button").addClass("active");
    $("#front-end .row, #back-end .row, #application .row, #professional .row").hide();
    $(id + " .row").fadeIn('slow');        
}


function drawGraphs() {
    var barChartData = {
        labels: ["HTML5/CSS3", "Foundation", "JS/Jquery", "SASS", "Bootstrap"],
        datasets: [
            {
                label: "Skills",
                fillColor: "#22435a", 
                highlightFill: "#305f80",
                data: [95, 90, 85, 65, 50]
            }
        ]
    };

    var ctx = document.getElementById("front-end-graph").getContext("2d");
    ctx.clearRect(0,0,ctx.height, ctx.width);
    var spacing = 20;
    if (Modernizr.mq('(max-width: 500px)')) {
        spacing = 1;
    }
    
    window.myObjBar = new Chart(ctx).Bar(barChartData, {
        responsive : true,
        barValueSpacing : spacing
    });

    //nuevos colores
    myObjBar.datasets[0].bars[1].fillColor = "#2b5673"; //bar 2
    myObjBar.datasets[0].bars[1].highlightFill = "#397399"; //bar 2
    myObjBar.datasets[0].bars[2].fillColor = "#306080"; //bar 3
    myObjBar.datasets[0].bars[2].highlightFill = "#3e7da6"; //bar 3
    myObjBar.datasets[0].bars[3].fillColor = "#397298"; //bar 4
    myObjBar.datasets[0].bars[3].highlightFill = "#478fbe"; //bar 4
    myObjBar.datasets[0].bars[4].fillColor = "#3e7ba4"; //bar 5
    myObjBar.datasets[0].bars[4].highlightFill = "#4c98ca"; //bar 5
    myObjBar.update();
}



function scrollTo(target) {
    var $target = $(target);        
    if(isSkill($target))
        $target = $("#skills");
    $('html, body').stop().animate({
        'scrollTop': $target.offset().top - 60
    }, 900, 'swing', function () {
        window.location.hash = target;
    });   
}

function isSkill($target) {
    var skills = [ "front-end", "back-end", "application", "professional" ];
    for (var i = 0; i < skills.length; i++) {
        if($target.attr("id") == skills[i])
            return true;
    }   
    return false;
}

// http://stackoverflow.com/questions/9979827/change-active-menu-item-on-page-scroll
// Thanks to Marcus Ekwall
// Cache selectors
var topMenu = $(".top-bar"),
    topMenuHeight = topMenu.outerHeight()+15,
    // All list items
    menuItems = topMenu.find("a"),
    // Anchors corresponding to menu items
    scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
    });

// Bind to scroll
$(window).scroll(function(){
    // Get container scroll position
    var fromTop = $(this).scrollTop()+topMenuHeight;
    

    // Get id of current scroll item
    var cur = scrollItems.map(function(){
        if ($(this).offset().top < fromTop)
            return this;
    });
    // Get the id of the current element
    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : "";
    
    //Check if we are at the bottom and overide contact highlight
    if($(window).scrollTop() + $(window).height() == $(document).height())
        id="contact";
    
    // Set/remove active class
    menuItems.parent().removeClass("active")
    .end().filter("[href=#"+id+"]").parent().addClass("active");
});

