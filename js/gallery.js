
var links = {
	big: {
		url: 'https://utsbig.com.au',
		txt: 'See the live website'
	},
	helps: {
		url: 'http://github.com/paul-pagnan/helps',
		txt: 'Check out the code!',
		buttonText: 'View on Github'
	},
	gp: {
		url: 'http://www.georgepagnan.com.au',
		txt: 'See the live website'
	},
	ticketman: {
		url: 'https://bitbucket.org/paul_pagnan/bigticketmanager',
		txt: 'Check out the code!',
		buttonText: 'View on Bitbucket'
	},
	praytoday: {
		url: 'https://bitbucket.org/paul_pagnan/praytoday',
		txt: 'Check out the code!',
		buttonText: 'View on Bitbucket'
	},
	washable: {
		url: 'https://bitbucket.org/paul_pagnan/washable',
		txt: 'Check out the code!',
		buttonText: 'View on Bitbucket'
	}
};

var images = {
	big: [
		{ src: 'images/big/big-lg-0.jpg',  w: 1848, h: 997, title: GetLink('big') },
		{ src: 'images/big/big-lg-1.jpg',  w: 1848, h: 997, title: GetLink('big') },
		{ src: 'images/big/big-lg-2.jpg',  w: 1848, h: 997, title: GetLink('big') },
		{ src: 'images/big/big-lg-3.jpg',  w: 373, h: 6359, title: GetLink('big') },
		{ src: 'images/big/big-lg-4.jpg',  w: 1848, h: 4393, title: GetLink('big') },
	],
	helps: [
		{ src: 'images/helps/helps-lg-0.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-1.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-2.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-3.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-4.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-5.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-6.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-7.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-8.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-9.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-10.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-11.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-13.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-14.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-15.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-16.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-17.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-19.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-20.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-21.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-22.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-23.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-24.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-25.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
		{ src: 'images/helps/helps-lg-26.jpg',  w: 1080, h: 1920, title: GetLink('helps') },
	],
	gp: [
		{ src: 'images/gp/gp-lg-0.jpg',  w: 1848, h: 997, title: GetLink('gp') },
		{ src: 'images/gp/gp-lg-1.jpg',  w: 1848, h: 1894, title: GetLink('gp') },
	],
	ticketman: [
		{ src: 'images/ticketman/ticketman-lg-0.jpg',  w: 1920, h: 1368, title: GetLink('ticketman')},
		{ src: 'images/ticketman/ticketman-lg-1.jpg',  w: 1920, h: 1780, title: GetLink('ticketman') },
		{ src: 'images/ticketman/ticketman-lg-2.jpg',  w: 1920, h: 935, title: GetLink('ticketman') },
		{ src: 'images/ticketman/ticketman-lg-3.jpg',  w: 1920, h: 1268, title: GetLink('ticketman') },
	],
	praytoday: [
		{ src: 'images/praytoday/praytoday-lg-0.jpg',  w: 1220, h: 772, title: GetLink('praytoday') },
		{ src: 'images/praytoday/praytoday-lg-1.jpg',  w: 1340, h: 848, title: GetLink('praytoday') },
		
	],
	washable: [
		{ src: 'images/washable/washable-lg-0.jpg',  w: 604, h: 300, title: GetLink('washable') },
		{ src: 'images/washable/washable-lg-1.jpg',  w: 1189, h: 658, title: GetLink('washable') },
		{ src: 'images/washable/washable-lg-2.jpg',  w: 1204, h: 658, title: GetLink('washable') },
	],
	spotify: [
		{ src: 'images/spotify/spotify-lg-0.jpg',  w: 1861, h: 996, title: GetLink('spotify') },
		{ src: 'images/spotify/spotify-lg-1.jpg',  w: 1861, h: 996, title: GetLink('spotify') },
		{ src: 'images/spotify/spotify-lg-2.jpg',  w: 1861, h: 996, title: GetLink('spotify') },
	],
};

var options = {};

function openGallery(type) {
	var pswpElement = document.querySelectorAll('.pswp')[0];
	var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, images[type], options);
    gallery.init();
}

function GetLink(type) {
	var lnk = links[type];
	if (!lnk) return '';
	var str = '';
	if (lnk.txt) 
		str += lnk.txt + '<br />';
	if (lnk.url) {
		str += '<a href="' + lnk.url + '" target="_blank" class="button small radius">';
		if (lnk.buttonText)
			str += lnk.buttonText;
		else
			str += 'Visit site';
		str += ' &nbsp; <i class="fa fa-external-link"></i></a>';
	}
	return str;

}